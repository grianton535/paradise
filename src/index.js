let botInfo = {}
if (process.env.npm_command !== "start") {
    require("dotenv").config({path: 'src/.env'})
    botInfo = require("../package.json")
} else {
    require("dotenv").config()
    botInfo = require("./package.json")
}

const VkBot = require('node-vk-bot-api');

const {Logger} = require("./utils")


const bot = new VkBot(process.env.TOKEN);

bot.command(['Начать', 'Start'], async ctx => { await ctx.reply('Привет')})
bot.on(async ctx => {
    await Logger.Message(ctx)
    console.log(ctx)
    await ctx.reply('Не понимаю')
})

try {
    bot.startPolling((err) => {
        if (err) {
            Logger.Error({message: `${err}`, from: "POLLING"});
            console.log(err)
        } else {
            Logger.Info({message: `${botInfo.name}:${botInfo.version} successfully started`})
        }
    });
} catch (e) {
    Logger.Error({message: `${e}`, from: "GLOBAL"})
}


